package unam.fi.poo.abstractas;

public class Triangulo extends Figura {
    private float lado1;
    private float lado2;
    private float lado3;

    public float calcularArea (){
	float semisuma;
	float area;
	semisuma = ( lado1 + lado2 +lado3 )/2;
	area = (float)Math.sqrt ( semisuma*( (semisuma-lado1) + ( semisuma-lado2 ) + ( semisuma-lado3 ) ) );
	return area;
	
    }
}
