package unam.fi.poo.abstractas;

import java.util.ArrayList;

public abstract class Figura {
    private ArrayList<Integer> parametros;

    public Figura (){
	parametros = new ArrayList<Integer>();
    }
    
    public abstract float calcularArea ();
}
